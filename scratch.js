function getNumber(promptText, defaultValue) {
    let number = parseFloat(prompt(promptText, defaultValue));
    if (isNaN(number)) {
        return getNumber(promptText, defaultValue);
    }
    return number;
}

let number1 = getNumber("Введіть перше число:", "");
let number2 = getNumber("Введіть друге число:", "");

let operation = prompt("Введіть математичну операцію (+, -, *, /):");

function calculate(number1, number2, operation) {
    let result;

    switch (operation) {
        case "+":
            result = number1 + number2;
            break;
        case "-":
            result = number1 - number2;
            break;
        case "*":
            result = number1 * number2;
            break;
        case "/":
            if (number2 === 0) {
                console.log("Ділення на нуль неможливе");
                return;
            }
            result = number1 / number2;
            break;
        default:
            console.log("Невідома операція");
            return;
    }

    console.log("Результат:", result);
}

calculate(number1, number2, operation);
